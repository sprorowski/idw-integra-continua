function soma( num1, num2){
    return num1 + num2;
}

describe('Soma valido teste',() => {
    it('1 + 2 = 3',() => {
        soma(1, 2) == 3;
    });
  });
  
describe('Soma valido2 teste',() => {
    it('2 + 2 = 4',() => {
        soma(2, 2) == 4;
    });
  });
  
describe('Soma invalido teste',() => {
    it('1 + 2 = 3',() => {
        !soma(1, 2) == 4;
    });
  });
  